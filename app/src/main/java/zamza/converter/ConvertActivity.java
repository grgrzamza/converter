package zamza.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class ConvertActivity extends AppCompatActivity {

    Button btnConvert;
    EditText editValue;
    TextView textResult;
    RadioButton radioUsd;
    RadioButton radioEur;
    RadioButton radioRus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convert);

        btnConvert = (Button) findViewById(R.id.btnConvert);
        editValue = (EditText) findViewById(R.id.editValue);
        textResult = (TextView) findViewById(R.id.textResult);
        radioUsd = (RadioButton) findViewById(R.id.radioUsd);
        radioEur = (RadioButton) findViewById(R.id.radioEur);
        radioRus = (RadioButton) findViewById(R.id.radioRus);
    }

    public void onBtnConvertClick(View v){
        double rate = GetCurrencyRate();
        double value;
        String strValue = editValue.getText().toString();
        if (strValue.isEmpty()){
            value = 1.0;
        } else{
            value = Double.parseDouble(strValue);
        }
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        textResult.setText(df.format(value / rate)) ;
    }

    private double GetCurrencyRate(){
        if (radioEur.isChecked()){
            return 2.06;
        } else if(radioUsd.isChecked()){
            return 1.95;
        } else if(radioRus.isChecked()){
            return 0.0315;
        } else{
            return 1;
        }
    }

}
